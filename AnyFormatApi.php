<?php

require_once './ReturnFormat.php';

class AnyFormatApi implements ReturnFormat {

    private $dataArray = [];

    public function __construct() {
        echo "Load AnyFormat API <br/>";
    }

    public function load() {
        // Some code and return array.
        return $this->dataArray;
    }
}
