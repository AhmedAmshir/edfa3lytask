<?php

require_once './Consumer.php';
require_once './XmlApi.php';
require_once './ArrayApi.php';
require_once './AnyFormatApi.php';

// Get Product data From ArrayApi And Save In DB
$consumer = new Consumer(new ArrayApi());
$data = $consumer->getProductData();
$consumer->saveInDB($data);

// Get Product data From XmlApi And Save In DB
$consumer = new Consumer(new XmlApi());
$data1 = $consumer->getProductData();
$consumer->saveInDB($data1);

// Get Product data From AnyFormatApi And Save In DB
$consumer = new Consumer(new AnyFormatApi());
$data2 = $consumer->getProductData();
$consumer->saveInDB($data2);