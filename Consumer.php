<?php

require_once './ReturnFormat.php';
require_once './XmlApi.php';
require_once './JsonApi.php';
require_once './ArrayApi.php';

class Consumer {
    private $product_data;

    public function __construct(ReturnFormat $data_formated) {
        $this->product_data = $data_formated;
    }

    public function getProductData() {
        $this->product_data->load();
    }

    public function saveInDB($data) {
        echo "Save Array Data In DB <br/>";
    }
}