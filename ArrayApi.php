<?php

require_once './ReturnFormat.php';

class ArrayApi implements ReturnFormat {

    private $dataArray = [];

    public function __construct() {
        echo "Load ArrayApi API <br/>";
    }

    public function load() {
        // Return array data.
        return $this->dataArray;
    }
}
